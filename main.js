/*var http = require("http");
http.createServer(function(request,responce) {
	//Send HTTP Header
	//Http Status: 200 : OK 
	//Contain type : text/plain
	responce.writeHead(200,{'Contain-Type':'text/plain'}) ;

	//send the responce Body as Hello world 
	responce.end('Hello worldw\n');
}).listen(8081);
//console will print msg
console.log("Server Running at http://127.0.0.1:8081/");
*/
//---------------------------------------------------------------------------
/*var fs = require("fs");
var data = fs.readFileSync("input.txt");
console.log(data.toString());
*/
//------------------------------------
/*var fs = require("fs");
fs.readFile("input.txt",function(err,data){
	if(err)
		console.log(err.stack,'error');
	else
		console.log(data.toString(),'success');
});
console.log("program ended");*/
//---------------------------------------------------------------------------
/*//import event module
var events = require("events");
//create an eventEmitter object
var eventEmitter = new events.EventEmitter();
//create an event handler as follow
var connectHandler = function connected(){
	console.log("connection successfull");
	//fire the data_received event
	eventEmitter.emit('data_received');
}
//bind the connection event with the handler
eventEmitter.on('connection',connectHandler);
//bind the data_received event with the anoymous function
eventEmitter.on('data_received',function(){
	console.log("data received successfully");
});
//Fire the connection event
eventEmitter.emit('connection');
console.log('Program Ended');*/
//------------------------------------
/*var events = require('events');
var eventEmitter = new events.EventEmitter();

// listener #1
var listner1 = function listner1() {
   console.log('listner1 executed.');
}

// listener #2
var listner2 = function listner2() {
  console.log('listner2 executed.');
}

// Bind the connection event with the listner1 function
eventEmitter.addListener('connection', listner1);

// Bind the connection event with the listner2 function
eventEmitter.on('connection', listner2);

var eventListeners = require('events').EventEmitter.listenerCount(eventEmitter,'connection');
console.log(eventListeners + " Listner(s) listening to connection event");

// Fire the connection event 
eventEmitter.emit('connection');

// Remove the binding of listner1 function
eventEmitter.removeListener('connection', listner1);
console.log("Listner1 will not listen now.");

// Fire the connection event 
eventEmitter.emit('connection');

eventListeners = require('events').EventEmitter.listenerCount(eventEmitter,'connection');
console.log(eventListeners + " Listner(s) listening to connection event");

console.log("Program Ended.");
*/
//---------------------------------------------------------------------------
/*buf = new Buffer(256);
len = buf.write("Simply Easy Learning");
console.log("Octets written : "+  len);
*/
/*
buf = new Buffer(26);
for (var i = 0 ; i < 26 ; i++) {
  buf[i] = i + 97;
}
console.log( buf.toString('ascii'));       // outputs: abcdefghijklmnopqrstuvwxyz
console.log( buf.toString('ascii',0,5));   // outputs: abcde
console.log( buf.toString('utf8',0,5));    // outputs: abcde
console.log( buf.toString(undefined,0,5)); // encoding defaults to 'utf8', outputs abcde
*/

/*var buf = new Buffer('Simply Easy Learning');
var json = buf.toJSON(buf);

console.log(buf.length);*/
//---------------------------------------------------------------------------
/*var fs = require("fs");
var data = '';
// Create a readable stream
var readerStream = fs.createReadStream('input.txt');
// Set the encoding to be utf8. 
readerStream.setEncoding('UTF8');
// Handle stream events --> data, end, and error
readerStream.on('data', function(chunk) {
   data += chunk;
});
readerStream.on('end',function(){
   console.log(data);
});
readerStream.on('error', function(err){
   console.log(err.stack);
});
console.log("Program Ended");*/
//---------------------------------------------------------------------------
/*var fs = require("fs");
var data = 'Simply Easy Learning';
// Create a writable stream
var writerStream = fs.createWriteStream('output.txt');
// Write the data to stream with encoding to be utf8
writerStream.write(data,'UTF8');

// Mark the end of file
writerStream.end();

// Handle stream events --> finish, and error
writerStream.on('finish', function() {
    console.log("Write completed.");
});

writerStream.on('error', function(err){
   console.log(err.stack);
});

console.log("Program Ended");
*/

/*var fs = require("fs");
// Asynchronous read
fs.readFile('input.txt', function (err, data) {
   if (err) {
      return console.error(err);
   }
   console.log("Asynchronous read: " + data.toString());
});
// Synchronous read
var data = fs.readFileSync('input.txt');
console.log("Synchronous read: " + data.toString());
console.log("Program Ended");*/

/*var fs = require("fs");
var buf = new Buffer(1024);
console.log("Going to open an existing file");
fs.open('input.txt', 'r+', function(err, fd) {
   if (err) {
      return console.error(err);
   }
   console.log("File opened successfully!");
   console.log("Going to read the file");
   fs.read(fd, buf, 0, buf.length, 0, function(err, bytes){
      if (err){
         console.log(err);
      }
      // Print only read bytes to avoid junk.
      if(bytes > 0){
         console.log(buf.slice(0, bytes).toString(),"dd");
      }
      // Close the opened file.
      fs.close(fd, function(err){
         if (err){
            console.log(err);
         } 
         console.log("File closed successfully.");
      });
   });
});
*/
//---------------------------------------------------------------------------
/*console.log(__filename);
console.log(__dirname);*/
//---------------------------------------------------------------------------
/*function printHello(){
	console.log("Hello YOgy");
}
//var t = setTimeout(printHello,5000);
var t = setInterval(printHello, 2000);
clearTimeout(t);*/
/*process.on('exit', function(code) {
   // Following code will never execute.
   setTimeout(function() {
      console.log("This will not run");
   }, 10);
  
   console.log('About to exit with code:', code);
});
console.log("Program Ended");*/
/*
// Print the current directory
console.log('Current directory: ' + process.cwd());

// Print the process version
console.log('Current version: ' + process.version);

// Print the memory usage
console.log(process.memoryUsage());*/

/*console.log(require("os").loadavg());*/

/*var path = require("path");

// Normalization
console.log('normalization : ' + path.normalize('/test/test1//2slashes/1slash/tab/..'));

// Join
console.log('joint path : ' + path.join('/test', 'test1', '2slashes/1slash', 'tab', '..'));

// Resolve
console.log('resolve : ' + path.resolve('main.js'));

// extName
console.log('ext name : ' + path.extname('main.js'));*/

var net = require('net');
var server = net.createServer(function(connection) { 
   console.log('client connected');
   
   connection.on('end', function() {
      console.log('client disconnected');
   });
   connection.write('Hello World!\r\n');
   connection.pipe(connection);
});
server.listen(8080, function() { 
   console.log('server is listening');
});